#!/usr/bin/env bash
node ./src/server/main.js &
npid=$!
# we currently don't need the frontend code
#npm run build &&
sleep 3 && NODE_TLS_REJECT_UNAUTHORIZED=0 ./node_modules/jasmine-xml-reporter/bin/jasmine.js $1 $2 spec/*Spec.js
kill $npid