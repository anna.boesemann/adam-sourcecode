/*
"use strict";
const frisby = require('frisby');
const Joi = frisby.Joi;
const jasmine = require('jasmine');
const axios = require('axios');

// start server
//require("../src/server/main.js");

// helper functions for the different requests
function postQuestionnaire(questionnaire, aid) {
    return frisby.post(`http://localhost:3000/api/questionnaires?aid=${aid}`, questionnaire);
}

function getAuthenticationToken(qid) {
    return frisby.get(`http://localhost:3000/api/questionnaires/${qid}/users/token`);
}

function getQuestionnaire(qid, uid) {
    return frisby.get(`http://localhost:3000/api/questionnaires/${qid}?uid=${uid}`);
}

function getAnswers(qid, uid) {
    return frisby.get(`http://localhost:3000/api/questionnaires/${qid}/answers?uid=${uid}`);
}

function postAnswers(qid, uid, answers) {
    return frisby
        .post(`http://localhost:3000/api/questionnaires/${qid}/answers?uid=${uid}`, answers);
}

const AdminToken = 'gWrjKR3oCyhZWEM032ABsgwk9RIY90';

function sleep(time) {
    return new Promise((resolve => {
        setTimeout(resolve);
    }))
}


describe(' REST API tests', function () {
    beforeAll(async function () {

    });

    it('post new questionnaire ', async function () {
        return postQuestionnaire(exampleQuestionnaire, AdminToken)
            .expect('status', 201)
            .expect('bodyContains', /[a-zA-Z0-9]{30}/);
    });

    it('post new questionnaire without valid admin token', async function () {
        return postQuestionnaire(exampleQuestionnaire, 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa')
            .expect('status', 403)
    });

    it('post new questionnaire without valid admin token', async function () {
        return postQuestionnaire(exampleQuestionnaire, '12@%^')
            .expect('status', 403)
    });

    it('add new user', async function () {
        let qid = (await postQuestionnaire(exampleQuestionnaire, AdminToken)).body;
        return getAuthenticationToken(qid)
            .expect('status', 201)
            .expect('bodyContains', /[a-zA-Z0-9]{30}/);
    });

    it('add new user without valid questionnaire id', async function () {
        return getAuthenticationToken('random')
            .expect('status', 404)
    });

    it('get default answerset for a questionnaire', async function () {
        let qid = (await postQuestionnaire(exampleQuestionnaire, AdminToken)).body;
        let uid = (await getAuthenticationToken(qid)).body;
        return getAnswers(qid, uid)
            .expect('status', 200);
    })

    it('post questionnaire and get it back', async function () {
        let qid = (await postQuestionnaire(exampleQuestionnaire, AdminToken)).body;
        let uid = (await getAuthenticationToken(qid)).body;
        //return getQuestionnaire(qid, uid)
        //     .expect('status', 200)
        //.expect('json', exampleQuestionnaire);
    });

    it('get a questionnaire that does not exist', async function () {
        return getQuestionnaire('randomqidxrandomqidxrandomqidx', 'randomuidxrandomuidxrandomuidx')
            .expect('status', 404)
    });

    it('post questionnaire and get it back without valid user', async function () {
        let qid = (await postQuestionnaire(exampleQuestionnaire, AdminToken)).body;
        let uid = (await getAuthenticationToken(qid)).body;
        return getQuestionnaire(qid, 'randomuidxrandomuidxrandomuidx')
            .expect('status', 200)
        //.expect('json', exampleQuestionnaire);
    });

    it('get default answerset and send it back', async function () {
        let qid = (await postQuestionnaire(exampleQuestionnaire, AdminToken)).body;
        let uid = (await getAuthenticationToken(qid)).body;
        let answerset = (await getAnswers(qid, uid)).body;
        let response = await axios.post(`http://localhost:3000/api/questionnaires/${qid}/answers?uid=${uid}`, answerset);
        return expect(response.status).toBe(200);
    })
});


const exampleQuestionnaire = {
    // no id field here because it's currently supplied by the server
    "id": 42,
    "surveytitle": "test survey",
    "starttime": 1528358766,
    "endtime": 1530000000,
    "welcomePage": "hallo du da? -> Welcoming Participant",
    "gdprNotice": "page for accepting GDPR Notice",
    "endPage": "Here you can insert your personal end-screen for now",
    "questionset": [
        {
            "id": 0,
            "questiontext": "What ist your gender?",
            "answers": [
                {
                    "id": 0,
                    "type": "radiobutton",
                    "text": "Select one option.",
                    options: [
                        "female",
                        "male",
                        {
                            "id": 1,
                            "type": "freetext"
                        }
                    ]
                },
                {
                    "id": 1,
                    "type": "radiobutton",
                    "text": "Select one option.",
                    options: [
                        "hello",
                        "bye"
                    ]
                },
                "answer-type"
            ]
        },
        {
            "id": 1,
            "questiontext": "How old are you?",
            "answers": [
                {
                    "id": 2,
                    "type": "freetext",
                    "text": "Please insert your age below."
                },
                {
                    "id": 3,
                    "type": "dropdown",
                    "options": ["1", "x"]
                }
            ]
        }
    ]
};
*/