#!/usr/bin/env bash
cd "$(dirname "$0")"
for i in `seq 1 25`;
do
    printf "https://umfragen.fg.informatik.uni-goettingen.de/?qid="
    NODE_TLS_REJECT_UNAUTHORIZED=0 node ./uploadHelper.js
done