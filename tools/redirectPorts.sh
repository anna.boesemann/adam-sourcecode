#!/usr/bin/env bash

if [ "$1" == "a" ]; then
    while true; do
        nc -L localhost:8080 -p 80
    done
    exit
fi
if [ "$1" == "b" ]; then
    while true; do
        nc -L localhost:8443 -p 443
    done
    exit
fi
if [ "$EUID" -ne 0 ]; then
    echo "please run as root"
else
    bash -c "$0 a" &
    p1=$!
    bash -c "$0 b" &
    p2=$!
    trap "kill $p1; kill $p2; exit" INT
    sleep 999999999999
fi