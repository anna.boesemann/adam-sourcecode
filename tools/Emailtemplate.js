let links = ['https://umfragen.fg.informatik.uni-goettingen.de/?qid=bzN8J2H3gnL0T1RtOtUIlHmHLJCUyq',
    'https://umfragen.fg.informatik.uni-goettingen.de/?qid=n4YeT0eV1o2psCx00yrW9r9ytlYgWC',
    'https://umfragen.fg.informatik.uni-goettingen.de/?qid=Ym35JlipduAKgE8Xt6SxPlSyKwuY04',
    'https://umfragen.fg.informatik.uni-goettingen.de/?qid=PFwJ6IsHh5Wbv5wqe3fqPWTzs8jQDY',
    'https://umfragen.fg.informatik.uni-goettingen.de/?qid=a4fBWp9x6Zp0MpyGrfMa8C2O8XyUFe',
    'https://umfragen.fg.informatik.uni-goettingen.de/?qid=j2nclacjPHsBMPhGRXbS8K3duXsR3y',
    'https://umfragen.fg.informatik.uni-goettingen.de/?qid=URFla5JagCTKnEJx0t0UAr5TXgWVDu',
    'https://umfragen.fg.informatik.uni-goettingen.de/?qid=C7jIBmydbdby7UY0ftQsM2t86djArX',
    'https://umfragen.fg.informatik.uni-goettingen.de/?qid=bjcozwL8VkzaQd9BQH6UFpwA1sj2Zu',
    'https://umfragen.fg.informatik.uni-goettingen.de/?qid=64AHC6T2WCSs2jEBRoysgonNrCibNW',
    'https://umfragen.fg.informatik.uni-goettingen.de/?qid=ED6l59kfzMc9T8GrOZp2pTkjxXaczl',
    'https://umfragen.fg.informatik.uni-goettingen.de/?qid=085jVSv2YtLixIoqjU1gqlTW6Vo718',
    'https://umfragen.fg.informatik.uni-goettingen.de/?qid=YI2ztr5m04oDeAfX17Mk7iQY3kQFCh',
    'https://umfragen.fg.informatik.uni-goettingen.de/?qid=KVkKnTsXGkGpJq402CiqCmJgVl1Ign',
    'https://umfragen.fg.informatik.uni-goettingen.de/?qid=feZYYwezf6i9c0UTpJVyaYPunpbgyj',
    'https://umfragen.fg.informatik.uni-goettingen.de/?qid=1toLlFpZ4VhxCh4serYnPvTGebf7zY',
    'https://umfragen.fg.informatik.uni-goettingen.de/?qid=ENhCNBLa7OvZDgYumTGuylgzo8WhYy',
    'https://umfragen.fg.informatik.uni-goettingen.de/?qid=L29cz6A7qNdVwMLe7XrJ1fBbqUbUUT',
    'https://umfragen.fg.informatik.uni-goettingen.de/?qid=SF20oOI628uCuXJiHmecCWLZ647l1D',
    'https://umfragen.fg.informatik.uni-goettingen.de/?qid=OnFFRUXZyP8vLAnax8h6G0i2I6pH6L',
    'https://umfragen.fg.informatik.uni-goettingen.de/?qid=xLv5b2WoGMNk8PiHgnH1VTuCEY5vhM',
    'https://umfragen.fg.informatik.uni-goettingen.de/?qid=2ZNRt4Xincl0wBEteCPgOkhXmZJVmr',
    'https://umfragen.fg.informatik.uni-goettingen.de/?qid=zbNlIoq3Ya6gFatU4lcAbjcOTrlYwT',
    'https://umfragen.fg.informatik.uni-goettingen.de/?qid=LtKXAa1kzxZmdMPXO5Ulk2vYi1X7cV',
    'https://umfragen.fg.informatik.uni-goettingen.de/?qid=7TSzozX1OSQYL4NcyinDbVLeEANbSb'];
let veranstaltungen = [
    {
        geschlecht: true,
        name: "Prof. Dr. Dr. Claus-Peter Czerny",
        email: "cczerny@gwdg.de",
        veranstaltung: "Grundlagen der Nutztierwissenschaften I"
    },
    {
        geschlecht: true,
        name: "Dr. Henrik Brosenne",
        email: "brosenne@informatik.uni-goettingen.de",
        veranstaltung: "Informatik II"
    },
    {
        geschlecht: false,
        name: "Prof. Dr. Krisztina Kis-Katos",
        email: "krisztina.kis-katos@uni-goettingen.de",
        veranstaltung: "Makroökonomik I"
    },
    {
        geschlecht: true,
        name: "Prof. Dr. Gerhard Rübel",
        email: "gruebel@uni-goettingen.de",
        veranstaltung: "Makroökonomik II"
    },
    {
        geschlecht: false,
        name: "Prof. Dr. Stefani Pöggeler",
        email: "spoegge@gwdg.de",
        veranstaltung: "Ringvorlesung Biologie II"
    },
    {
        geschlecht: true,
        name: "Prof. Dr. Lutz Ackermann",
        email: "lutz.ackermann@chemie.uni-goettingen.de",
        veranstaltung: "Einführung in die Organische Chemie: Experimentalchemie II"
    },
    {
        geschlecht: true,
        name: "Prof. Dr. Florian Wilk",
        email: "Florian.Wilk@theologie.uni-goettingen.de",
        veranstaltung: "Einführung in das Neue Testament"
    },
    {
        geschlecht: true,
        name: "Prof. Dr. Thomas Mann",
        email: "sekretariatmann@jura.uni-goettingen.de",
        veranstaltung: "Staatsrecht II"
    },
    {
        geschlecht: false,
        name: "Prof. Dr. Chenchang Zhu",
        email: "chenchang.zhu@mathematik.uni-goettingen.de ",
        veranstaltung: "Analytische Geometrie und Lineare Algebra II"
    },
    {
        geschlecht: true,
        name: "PD Dr. Dolf Rami",
        email: "drami@gwdg.de",
        veranstaltung: "Einführung in die Logik"
    },
    {
        geschlecht: false,
        name: "Prof. Dr. Dorothea Bahns",
        email: "Dorothea.Bahns@mathematik.uni-goettingen.de",
        veranstaltung: "Mathematik für Studierende der Physik II"
    },
    {
        geschlecht: true,
        name: "Prof. Dr. Hannes Rakoczy",
        email: "hrakocz@uni-goettingen.de",
        veranstaltung: "Entwicklungspsychologie"
    },
    {
        geschlecht: false,
        name: "Prof. Dr. Nicole Mayer-Ahuja",
        email: "nicole.mayer-ahuja@sowi.uni-goettingen.de",
        veranstaltung: "Einführung in die Soziologie von Unternehmen, Arbeit, Wirtschaft"
    },
    {
        geschlecht: true,
        name: "Prof. Dr. Matthias Schumann",
        email: "mschuma1@uni-goettingen.de",
        veranstaltung: "Unternehmen und Märkte"
    },
    {
        geschlecht: true,
        name: "Prof. Dr. med. Jochen Staiger",
        email: "jochen.staiger@med.uni-goettingen.de",
        veranstaltung: "Grundlagen der Anatomie"
    },
    {
        geschlecht: false,
        name: "Prof. Dr. Claudia Steinem",
        email: "claudia.steinem@chemie.uni-goettingen.de",
        veranstaltung: "Chemie für Studierende der Human-/ Zahnmedizin"
    },
];

// Betreff: `Mithilfe bei Umfrage bzgl. Selbststudium
function mail(d, link) {
    return `To: <${d.email}>,
Subject: Mithilfe bei Umfrage bzgl. Selbststudium
Content-Type: text/plain; charset="UTF-8"
` + '\n' + (d.geschlecht ? `Sehr geehrter Herr ${d.name},` :
        `Sehr geehrte Frau ${d.name},`) + `

wir sind eine Gruppe von 5 Studierenden, die im Rahmen einer Universitätsveranstaltung des Masters 
in Angewandter Informatik eine Onlineumfrage zum Thema "Selbstorganisation und Informationsgewinnung 
von Studierenden im Studium" durchführt. 
Wir möchten Sie deshalb bitten, dass Sie den folgenden Link oder diese Mail an die Teilnehmenden 
Ihrer Veranstaltung "${d.veranstaltung}" weiterzuleiten.

${link}

Die Ergebnisse dieser Umfrage könnten Sie auch interessieren. Daher stellen wir diese Ihnen gern zur 
Verfügung. Antworten Sie dafür bitte kurz auf diese Mail.

Vielen Dank im Voraus,
Lorenz Glißmann
im Namen der kompletten Projektgruppe.`;
}

let fs = require('fs');

for (let i = 0; i < veranstaltungen.length; i++) {
    fs.writeFileSync(`mails/email${i}.eml`, mail(veranstaltungen[i], links[i]))
}
