let createError = require('http-errors');
let express = require('express');
let path = require('path');
let logger = require('morgan');
const log4js = require('log4js');

let apiRouter = require('./routes/api');

let app = express();

log4js.configure('config/log4js.json');
const errorLogger = log4js.getLogger('error');
const apiLogger = log4js.getLogger('api');

app.use(logger(':remote-user ":method :url HTTP/:http-version" :status :res[content-length] ":referrer"":user-agent"', {stream: {write: message => apiLogger.trace(message.trim())}}));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(express.static(path.join(__dirname, '/../../public')));

app.use('/api', apiRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    errorLogger.error('wrong url');
    //console.log('wrong url')
    next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    res.sendStatus(err.status || 500);
    errorLogger.error(err);
});

function addReplContext(repl) {
    apiRouter.addReplContext(repl);
}

module.exports = app;
module.exports.addReplContext = addReplContext;